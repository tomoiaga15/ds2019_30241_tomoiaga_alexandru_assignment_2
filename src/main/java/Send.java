import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Send {

    private final static String QUEUE_NAME = "activity";

    public static void main(String[] argv) throws Exception {
        BufferedReader reader;
        Random rand = new Random();
        ObjectMapper mapper = new ObjectMapper();
        Activity activity = new Activity();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            reader = new BufferedReader(new FileReader("src/main/resources/activity.txt"));
            String line = reader.readLine();
            while (line != null) {
                String[] strings = line.split("\\t+");
                activity.setStartTime(strings[0]);
                activity.setEndTime(strings[1]);
                activity.setActivityName(strings[2]);
                activity.setPatientId(rand.nextInt(2)+1);
                String toJson = mapper.writeValueAsString(activity);
                channel.basicPublish("", QUEUE_NAME, null, toJson.getBytes("UTF-8"));
                System.out.println(" [x] Sent '" + toJson + "'");
                TimeUnit.MILLISECONDS.sleep(100);
                line = reader.readLine();
            }
            reader.close();
        }
    }
}